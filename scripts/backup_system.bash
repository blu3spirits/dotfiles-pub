#!/bin/bash

# Author: Blu3spirits

TIMESTAMP="$(date +"%a-%b-%d-%Y")"
BACKUPDIRPARENT="/home/spirits/mount/hdd/backups"
BACKUPDIR="${BACKUPDIRPARENT}/${TIMESTAMP}"
BACKUPFILE="${TIMESTAMP}-backup.tar.bz2"
LOGFILE="${BACKUPDIRPARENT}/${TIMESTAMP}-backup.log"
TARGETROOT="/"

mkdir -p "${BACKUPDIR}"
echo "Starting backup @ ${BACKUPDIR}/${BACKUPFILE}" >> "${LOGFILE}"
rsync --exclude=/mnt/ \
--exclude=/sys/ \
--exclude=/proc/ \
--exclude=/home/spirits/mount/ \
--exclude=/dev -Pavz "${TARGETROOT}" "${BACKUPDIR}" | tee -a "${LOGFILE}"
# Backup the entire directory to the backup location
echo "Backup done" >> "${LOGFILE}"
echo "Compressing and removing" >> "${LOGFILE}"
tar -cjvf "${BACKUPDIRPARENT}/${BACKUPFILE}" "${BACKUPDIR}" | tee -a "${LOGFILE}"
rm -rvf "${BACKUPDIR}" | tee -a "${LOGFILE}"

ls "${BACKUPDIRPARENT}/${BACKUPFILE}" >> "${LOGFILE}"
file "${BACKUPDIRPARENT}/${BACKUPFILE}" >> "${LOGFILE}"

echo "Removing backups greater than 7 days old" >> "${LOGFILE}"
find "${BACKUPDIRPARENT}" -type f -mtime +7 -name "*.bz2" -execdir rm -vf -- '{}' \;

echo "Script done" >> "${LOGFILE}"
