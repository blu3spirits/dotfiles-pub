#!/bin/bash
HOME=$HOME
DOTFILES_DIR_SOURCE="$PWD/../"
DOTFILES_FILES=$(ls -m ${DOTFILES_DIR_SOURCE})
echo "Installing dotfiles into ${HOME}"

if [ ! -d "$HOME" ]; then
    echo "creating folder"
    mkdir -v $HOME
fi

readarray -t VALID_FILES < <(ls -m $DOTFILES_DIR_SOURCE | grep -v --exclude-dir * | tr -d ',')
for x in $VALID_FILES
do
    echo $x .$x
    rsync -av --exclude="scripts" $DOTFILES_DIR_SOURCE/$x $HOME/.$x
done
