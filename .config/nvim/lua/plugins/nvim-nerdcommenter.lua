-----------------------------------------------------------
-- File manager configuration file
-----------------------------------------------------------

-- Plugin: nvim-tree
-- url: https://github.com/preservim/nerdcommenter

local status_ok, nerdcommenter = pcall(require, 'nerdcommenter')
if not status_ok then
  return
end

-- Call setup:
-- Each of these are documented in `:help nerdcommenter`
nerdcommenter.setup{}
