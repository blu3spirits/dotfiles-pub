# NeoVim config

### Packages required

```bash
sudo pacman -S neovim
```

### Language servers
```bash
sudo npm install -g bash-language-server pyright vscode-langservers-extracted typescript typescript-language-server
sudo pacman -S rust-analyzer
```

### Keybinds
| Key | Description |
| :--- | -----------: |
| `,q`  | Force quit out |
| `CTRL+{h,j,k,l}`| Move around splits in vim |
| `,r`| Reload vim window without restart |
| `CTRL+n` | Toggle Nerd Tree |
| `,f` | Refresh Nerd tree |
| `,n` | Nerdtree find file |
| `,z` | Toggle Tagbars |
| `,F` | Find file with telescope |
| `CTRL+g` | live grep through files with telescope |

### References

Based heavily on:

https://github.com/brainfucksec/neovim-lua
